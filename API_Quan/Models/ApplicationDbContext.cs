﻿using API_Quan.Model;
using Microsoft.EntityFrameworkCore;

namespace API_Quan.Models
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext>options) : base(options)
        {
        }
        public DbSet<Product> Products { get; set; }
    }
}
